var N = 10
var MAX_N = 50

var colorPalette = [0xff0000, 0x00ff00, 0x0000ff, 0xffff00, 0xff00ff, 0x00ffff, 0x888800, 0x880088]

let boxPositions = [
    new THREE.Vector3(1, 1, 1),
    new THREE.Vector3(-1, 1, 1),
    new THREE.Vector3(1, -1, 1),
    new THREE.Vector3(-1, -1, 1),
    new THREE.Vector3(1, 1, -1),
    new THREE.Vector3(-1, 1, -1),
    new THREE.Vector3(1, -1, -1),
    new THREE.Vector3(-1, -1, -1)
]

let boxEdgesIdx = [
    [0, 1],
    [2, 3],
    [0, 2],
    [1, 3],

    [4, 5],
    [6, 7],
    [4, 6],
    [5, 7],

    [0, 4],
    [1, 5],
    [2, 6],
    [3, 7]
]


class InteractiveBox {
    constructor(cubePosition, cubeRotationX, cubeRotationY) {
        this.points = []
        this.edges = []

        for (let i = 0; i < boxPositions.length; i++) {
            let geometry = new THREE.SphereBufferGeometry(0.1, 8, 8).translate(boxPositions[i].x, boxPositions[i].y, boxPositions[i].z);
            let material = new THREE.MeshBasicMaterial({
                color: colorPalette[i]
            })
            let sphere = new THREE.Mesh(geometry, material)

            this.points.push(sphere)
            scene.add(sphere);
        }

        for (let i = 0; i < boxEdgesIdx.length; i++) {
            var points = [boxPositions[boxEdgesIdx[i][0]], boxPositions[boxEdgesIdx[i][1]]];

            let material = new THREE.MeshBasicMaterial({
                color: 0x000000
            })
            let geometry = new THREE.BufferGeometry().setFromPoints(points);
            let line = new THREE.Line(geometry, material)

            this.edges.push(line)
            scene.add(line);
        }

        this.points.forEach(function(obj) {
            obj
                .translateX(cubePosition.x)
                .translateY(cubePosition.y)
                .translateZ(cubePosition.z)
                .rotateX(cubeRotationX)
                .rotateY(cubeRotationY)
        });

        this.edges.forEach(function(obj) {
            obj
                .translateX(cubePosition.x)
                .translateY(cubePosition.y)
                .translateZ(cubePosition.z)
                .rotateX(cubeRotationX)
                .rotateY(cubeRotationY)
        });
    }

    renderClear() {
        this.points.forEach(function(obj) {
            scene.remove(obj)
        });

        this.edges.forEach(function(obj) {
            scene.remove(obj)
        });
    }

    mouseClickProcess() {
        raycaster.setFromCamera(mouse, camera);
        this.points.forEach(function(point, i) {
            if (raycaster.intersectObject(point).length == 0) {
                return;
            }

            let pointColor = point.material.color;
            this.edges.forEach(function(edge, r) {
                if (boxEdgesIdx[r][0] == i || boxEdgesIdx[r][1] == i) {
                    edge.material.color = pointColor;
                }
            });
        }, this);
    }
}


var vueApp;

var scene;
var camera;
var mouse;
var renderer;
var raycaster;

var boxes;


initVue();
initThreeJs();
initListeners();
initBoxes();


function animate() {
    requestAnimationFrame(animate);

    var speed = Date.now() * 0.000025;
    camera.position.x = Math.cos(speed) * 15;
    camera.position.z = Math.sin(speed) * 15;
    camera.lookAt(new THREE.Vector3(0, 0, 0));

    renderer.render(scene, camera);
}
animate();


//--------------------------------------------------------------------------------------------------------

function initVue() {
    vueApp = new Vue({
        el: "#N",
        data: {
            value: N
        },
        methods:{
            signalChange:function(event){
                this.value = updateN(this.value);
            }
        }
    });
}

function initThreeJs() {
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xffffff);
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    mouse = new THREE.Vector2();

    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.domElement.style.position = 'absolute';
    renderer.domElement.style.left = '0px';
    renderer.domElement.style.top = '0px';
    renderer.domElement.style['z-index'] = '-2';
    document.body.appendChild(renderer.domElement);

    raycaster = new THREE.Raycaster();
}

function initListeners() {
    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('mousedown', onDocumentMouseDown, false);
}

function initBoxes() {
    boxes = []
    for (let i = 0; i < N; i++) {
        boxes.push(new InteractiveBox(new THREE.Vector3(Math.random() * 14 - 7, Math.random() * 14 - 7, Math.random() * 14 - 7), Math.random(), Math.random()))
    }
}


function updateN(newN) {
    this.boxes.forEach(function(box) {
        box.renderClear();
    });
    N = Math.max(Math.min(newN, MAX_N), 0);
    initBoxes();
    return N;
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function onDocumentMouseMove(event) {
    event.preventDefault();

    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
}

function onDocumentMouseDown(event) {
    boxes.forEach(function(box) {
        box.mouseClickProcess();
    });
}